﻿using System;
using System.Windows.Forms;
using log4net;

namespace WordSkipper
{
    static class Program
    {
        public static readonly ILog Log = LogManager.GetLogger(typeof(Program));
        public static readonly string AppVersion = " v 1.05";

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            log4net.Config.XmlConfigurator.Configure();

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new MainForm());
        }
    }
}
