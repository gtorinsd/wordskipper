﻿using System;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Windows.Forms.VisualStyles;

namespace WordSkipper.Workers
{
    public enum LettersReplaceTypes
    {
        Vowes,
        Consonants,
        Randomly
    }

    public class LettersWorker
    {
        private const char m_CharToReplaceText = '_';
        private const char m_Apostrophy = '\'';

        private const string m_ArrVowel = "aeioyu";

        private int GetHidesCount(string strIn, int count, LettersReplaceTypes replaceType = LettersReplaceTypes.Randomly)
        {
            int realCount = count;
            if (strIn.Length - 2 < realCount)
            {
                realCount = strIn.Length - 2;
            }

            switch (replaceType)
            {
                case LettersReplaceTypes.Vowes:
                {
                    int targetLettersCount = strIn.Where((t, i) => m_ArrVowel.IndexOf(t) >= 0 && i > 0 && i < strIn.Length - 1).Count();
                    if (targetLettersCount < realCount)
                    {
                        realCount = targetLettersCount;
                    }
                    break;
                }
                case LettersReplaceTypes.Consonants:
                {
                    int targetLettersCount = strIn.Where((t, i) => m_ArrVowel.IndexOf(t) < 0 && i > 0 && i < strIn.Length - 1 && t != m_CharToReplaceText && t != m_Apostrophy).Count();
                    if (targetLettersCount < realCount)
                    {
                        realCount = targetLettersCount;
                    }
                    break;
                }
                case LettersReplaceTypes.Randomly:
                {
                    int targetLettersCount = strIn.Where((t, i) => i > 0 && i < strIn.Length - 1 && t != m_CharToReplaceText && t != m_Apostrophy).Count();
                    if (targetLettersCount < realCount)
                    {
                        realCount = targetLettersCount;
                    }
                    break;
                }
                default:
                {
                    throw new ArgumentOutOfRangeException(nameof(replaceType), replaceType, null);
                }
            }

            return realCount;
        }

        public string HideLetters(string strIn, int minWordLen, int count, LettersReplaceTypes replaceType = LettersReplaceTypes.Randomly)
        {
            string result = strIn;
            var punctuation = result.Where(char.IsPunctuation).Distinct().ToArray();
            var words = result.Split().Where(x=> !string.IsNullOrEmpty(x.Trim(punctuation)) && x.Trim(punctuation).Length >= minWordLen).Select(x => x.Trim(punctuation)).Distinct();

            Random rnd = new Random();
            foreach (string item in words)
            {
                int realCount = GetHidesCount(item, count, replaceType);
                StringBuilder sb = new StringBuilder(item);
                for (int i = 0; i < realCount; i++)
                {
                    // Indexes may be duplicated
                    bool replaceOk = false;
                    while (!replaceOk)
                    {
                        int index = rnd.Next(1, item.Length - 1);
                        if (sb[index] == m_CharToReplaceText || sb[index] == m_Apostrophy)
                        {
                            continue;
                        }

                        switch (replaceType)
                        {
                            case LettersReplaceTypes.Vowes:
                            {
                                if (m_ArrVowel.IndexOf(sb[index]) >= 0)
                                {
                                    sb[index] = m_CharToReplaceText;
                                    replaceOk = true;
                                }
                                break;
                            }
                            case LettersReplaceTypes.Consonants:
                            {
                                if (m_ArrVowel.IndexOf(sb[index]) < 0)
                                {
                                    sb[index] = m_CharToReplaceText;
                                    replaceOk = true;
                                }
                                break;
                            }
                            default:
                            {
                                // Randomly
                                sb[index] = m_CharToReplaceText;
                                replaceOk = true;
                                break;
                            }
                        }
                    }
                }

                string pattern = @"\b" + item + @"\b";
                result = Regex.Replace(result, pattern, sb.ToString());
            }
            return result;
        }
    }
}
