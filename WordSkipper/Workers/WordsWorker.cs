﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text.RegularExpressions;
using System.Linq;
using log4net;

namespace WordSkipper
{
    class WordsWorker
    {
        #region Class members
        private const char m_CharToReplaceText = '_';
        private readonly char[] m_ArrayWordsSplitter = { ' ', ',', '.', ':', ';', '?', '!', '\n', '\'', '\"', '(', ')', '[', ']', '\\', m_CharToReplaceText };
        private const string m_WholeWordRegexpTemplate = "\\b{0}\\b";

        // Total words count
        private int m_TotalWordsCount;
        // Logger
        private static readonly ILog m_log = LogManager.GetLogger(typeof(WordsWorker));
        #endregion

        public int TotalWordsCount
        {
            get => m_TotalWordsCount;
        }

        public string[] SplitTextToWords(String text)
        {
            // Split text to words and put them to array
            string[] arr = text.Split(m_ArrayWordsSplitter, StringSplitOptions.RemoveEmptyEntries);

            // Total words count in the text
            m_TotalWordsCount = arr.Length;
            m_log.Debug($"Count of word in text is {m_TotalWordsCount}");

            // Remove duplicates from the array, remove numbers from array
            int n;
            arr = arr.Distinct(StringComparer.CurrentCultureIgnoreCase).Where(val => !int.TryParse(val, out n)).ToArray();

            m_log.Debug($"Count of word in list is {arr.Length}");
            m_log.Debug($"Words are: [{String.Join(", ", arr)}]");
            return arr;
        }

        public string RemoveWordsFromTheTextRandomly(string fromText, int wordsCountToRemove, out int removedWordsCount)
        {
            // remove N any words randomly
            string text = fromText;
            string[] arr;
            removedWordsCount = 0;
            Random rnd = new Random();


            arr = SplitTextToWords(text);

            m_log.Debug($"Replacing {wordsCountToRemove} words");
            for (int i = 0; i < wordsCountToRemove; i++)
            {
                if (arr.Length == 0)
                {
                    m_log.Debug("arr.Length is 0");
                    break;
                }

                removedWordsCount++;

                Trace.WriteLine($"===== Total words count: {m_TotalWordsCount}");

                // Get next randomed index or the word in the array
                int pos = rnd.Next(arr.Length);
                // Probably, there are control chars in the string. So we should use "@"
                string wordToRemove = @arr[pos];
                // trying to find position on the word in the text
                int j = text.IndexOf(wordToRemove, StringComparison.Ordinal);
                if (j >= 0)
                {
                    //*******************************************
                    // Replace wholy words only,  but ony one word per turn
                    // This is an exception place
                    Regex reg = new Regex(string.Format(m_WholeWordRegexpTemplate, wordToRemove));
                    text = reg.Replace(text, new string(m_CharToReplaceText, wordToRemove.Length), 1);
                    //*******************************************
                }
                else
                {
                    m_log.Error($"Unable to find the word: \"{wordToRemove}\"");
                }
                arr = SplitTextToWords(text);
            }
            return text;
        }

        public string RemoveWordFromTheText(string fromText, string wordToRemove, out int removedWordsCount)
        {
            // Replace wholy words only
            // This is an exception place
            Regex reg = new Regex(String.Format(m_WholeWordRegexpTemplate, wordToRemove), RegexOptions.IgnoreCase);
            string text = reg.Replace(fromText, new string(m_CharToReplaceText, wordToRemove.Length));

            //We need count of replaced strings :(
            removedWordsCount = reg.Matches(fromText).Count;
            SplitTextToWords(text);
            return text;
        }

        public IList<int> GetAllWordPositions(string word, string fromText)
        {
            if (m_ArrayWordsSplitter.Contains(word[0]))
            {
                word = "\\" + word;
            }

            IList<int> list = new List<int>();
            Regex reg = new Regex(string.Format(m_WholeWordRegexpTemplate, word), RegexOptions.IgnoreCase);
            Match m = reg.Match(fromText);
            while (m.Success)
            {
                list.Add(m.Groups[0].Captures[0].Index);
                m = m.NextMatch();
            }
            return list;
        }
    }
}
