﻿using System;
using System.Drawing;
using System.Drawing.Printing;
using System.IO;
using System.Windows.Forms;
using WordSkipper.Properties;

namespace WordSkipper
{
    public sealed class ReportWorker
    {
        #region Private fields
        private readonly RichTextBox _richText;
        private readonly PrintPreviewDialog _printPreviewDialog = new PrintPreviewDialog();
        private readonly PrintDialog _printDialog = new PrintDialog();
        private readonly PrintDocument _printDocument = new PrintDocument();
        private readonly PageSetupDialog _pageSetupDialog = new PageSetupDialog();
        private string _stringToPrint;
        #endregion

        public ReportWorker(RichTextBox textbox)
        {
            _richText = textbox;
            _printDocument.PrintPage += printDocument_PrintPage;

            Margins margins = new Margins { Top = Round(15), Bottom = Round(15), Left = Round(25), Right = Round(15) };
            _printDocument.DefaultPageSettings.Margins = margins;

            _pageSetupDialog.Document = _printDocument;
            _pageSetupDialog.EnableMetric = true;
            _printDialog.UseEXDialog = true;
            _printPreviewDialog.Document = _printDocument;

            AddButtonsToPrintDialog();
            _printPreviewDialog.WindowState = FormWindowState.Maximized;
        }

        private static int Round(float f)
        {
            return Convert.ToInt32(Math.Round(f * 3.93750011920929));
        }

        private void AddButtonsToPrintDialog()
        {
            ToolStripButton b = new ToolStripButton
            {
                Image = ((ToolStrip)(_printPreviewDialog.Controls[1])).ImageList.Images[0],
                DisplayStyle = ToolStripItemDisplayStyle.Image,
                Text = "Print..."

            };
            b.Click += PrintButtonClick;

            ((ToolStrip)(_printPreviewDialog.Controls[1])).Items.RemoveAt(0);
            ((ToolStrip)(_printPreviewDialog.Controls[1])).Items.Insert(0, b);

            var btn = ((ToolStrip)(_printPreviewDialog.Controls[1])).Items[9];

            b = new ToolStripButton
            {
                Image = Resources.RULERS,
                DisplayStyle = ToolStripItemDisplayStyle.Image,
                Text = "Page setup"

            };
            b.Click += PageSetupButtonClick;

            ((ToolStrip)(_printPreviewDialog.Controls[1])).Items.Insert(1, b);
        }

        public void PrintText()
        {
            _stringToPrint = _richText.Text;
            _printPreviewDialog.ShowDialog();
        }

        private void printDocument_PrintPage(object sender, PrintPageEventArgs e)
        {
            int charactersOnPage = 0;
            int linesPerPage = 0;

            // Sets the value of charactersOnPage to the number of characters 
            // of stringToPrint that will fit within the bounds of the page.
            //string stringToPrint = _richText.Text;

            e.Graphics.MeasureString(_stringToPrint, _richText.Font,
                e.MarginBounds.Size, StringFormat.GenericTypographic,
                out charactersOnPage, out linesPerPage);

            // Draws the string within the bounds of the page.
            e.Graphics.DrawString(_stringToPrint.Substring(0, charactersOnPage), _richText.Font, Brushes.Black,
            e.MarginBounds, StringFormat.GenericTypographic);

            // Remove the portion of the string that has been printed.
            _stringToPrint = _stringToPrint.Substring(charactersOnPage);

            // Check to see if more pages are to be printed.
            e.HasMorePages = (_stringToPrint.Length > 0);
        }

        private void PageSetupButtonClick(object sender, EventArgs e)
        {
            _pageSetupDialog.PageSettings.Margins = _printDocument.DefaultPageSettings.Margins;

            if (_pageSetupDialog.ShowDialog() != DialogResult.OK) return;
            if (_pageSetupDialog.PageSettings == null) return;

            _printDocument.DefaultPageSettings.Margins = _pageSetupDialog.PageSettings.Margins;
            _printPreviewDialog.PrintPreviewControl.InvalidatePreview();
        }

        private void PrintButtonClick(object sender, EventArgs e)
        {
            try
            {
                //_printDialog.StartPosition;
                if (_printDialog.ShowDialog() == DialogResult.OK)
                {
                    _printDocument.Print();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ToString());
            }            
        }
    }
}
