﻿using System;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using System.Collections.Generic;
using System.Diagnostics;
using System.Configuration;
using log4net;
using WordSkipper.Workers;

namespace WordSkipper
{
    public partial class MainForm : Form
    {
        #region Private members
        private WordsWorker m_Work = new WordsWorker();
        private const string m_RemainedWords = "Remained words: {0}";
        private const string m_RemovedWords = "Removed words: {0}";
        private const string m_ChangesCount = "Total changes: {0}";
        private string m_FormCaption;
        private bool m_AllowRichTextChanges;
        
        // Undo buffer for the richText
        private Stack<string> m_RichTextUndoList = new Stack<string>();

        // Log4net
        private static readonly ILog m_log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        #endregion

        public MainForm()
        {
            InitializeComponent();
            SetMinSize();
            InitContextMenuItemsTextAndIcon();

            m_log.Debug(String.Format("===== {0}{1} =====", System.Reflection.Assembly.GetEntryAssembly().GetName().Name, Program.AppVersion));
            ProcessTextAfterOpen();

            this.Text = System.Reflection.Assembly.GetEntryAssembly().GetName().Name + Program.AppVersion;
            
            m_FormCaption = this.Text;
            m_AllowRichTextChanges = true;

            SetButtonsCursors();
        }

        private void SetButtonsCursors()
        {
            foreach (var item in toolStrip.Items)
            {
                if (item is ToolStripButton)
                {
                    (item as ToolStripButton).MouseEnter += ToolStripButton_MouseEnter;
                    (item as ToolStripButton).MouseLeave += ToolStripButton_MouseLeave;
                }
            }
        }

        #region Events handlers

        #region Buttons
        private void toolStripButtonOpen_Click(object sender, EventArgs e)
        {
            DialogResult dl = openFileDialog.ShowDialog();
            if (dl == DialogResult.OK)
            {
                OpenFile(openFileDialog.FileName);
            }
        }

        private void toolStripButtonHideSelectedWord_Click(object sender, EventArgs e)
        {
            HideSelectedWord();
        }

        private void toolStripButtonFilter_Click(object sender, EventArgs e)
        {
            using (FormFilter formFilter = new FormFilter())
            {
                formFilter.Icon = Icon.FromHandle(new Bitmap(toolStripButtonFilter.Image).GetHicon());
                DialogResult result = formFilter.ShowDialog();
                if (result == DialogResult.OK)
                {
                    HideWordsRandomly(formFilter.GetWordsCount());
                }
            }
        }

        private void toolStripButtonHideLettersInAllWords_Click(object sender, EventArgs e)
        {
            FormHideLettersParams form = new FormHideLettersParams();
            form.Icon = Icon.FromHandle(new Bitmap(toolStripButtonHideLettersInAllWords.Image).GetHicon());
            DialogResult dl = form.ShowDialog();
            if (dl == DialogResult.OK)
            {
                HideLettersInAllWords(form.SelectedItem, form.HideLettersCount, 3);
            }
        }

        private void toolStripButtonSetWordColor_Click(object sender, EventArgs e)
        {
            DialogResult dlg = colorDialog.ShowDialog();
            if (dlg == DialogResult.OK)
            {
                SetSelectedTextColor(richTextBox.SelectedText.Trim(), colorDialog.Color);
            }
        }

        private void toolStripButtonTextBackGround_Click(object sender, EventArgs e)
        {
            colorDialog.Color = richTextBox.BackColor;
            DialogResult dl = colorDialog.ShowDialog();
            if (dl == DialogResult.OK)
            {
                richTextBox.BackColor = colorDialog.Color;
            }
        }

        private void toolStripButtonFont_Click(object sender, EventArgs e)
        {
            m_AllowRichTextChanges = false;
            try
            {
                fontDialog.Font = new Font(richTextBox.Font.Name, richTextBox.Font.Size, richTextBox.Font.Style);
                fontDialog.Color = richTextBox.ForeColor;

                DialogResult dl = fontDialog.ShowDialog();
                if (dl == DialogResult.OK)
                {
                    richTextBox.Font = new Font(fontDialog.Font.Name, fontDialog.Font.Size, fontDialog.Font.Style);
                    richTextBox.ForeColor = fontDialog.Color;
                }
            }
            finally
            {
                m_AllowRichTextChanges = true;
            }
        }

        private void toolStripUndo_Click(object sender, EventArgs e)
        {
            RichTextPopChanges();
        }

        private void toolStripButtonPrint_Click(object sender, EventArgs e)
        {
            PrintText();
        }

        private void toolStripButtonOptions_Click(object sender, EventArgs e)
        {
            using (Form formOptions = new FormOptions())
            {
                formOptions.Icon = Icon.FromHandle(new Bitmap(toolStripButtonOptions.Image).GetHicon());
                formOptions.ShowDialog();
            }
        }

        private void toolStripButtonExit_Click(object sender, EventArgs e)
        {
            Close();
        }
        #endregion

        private void richTextBox_TextChanged(object sender, EventArgs e)
        {
            RichTextPushChanges(richTextBox.Text);
        }

        private void richTextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Z && (e.Control))
            {
                RichTextPopChanges();
            }
        }

        private void richTextBox_SelectionChanged(object sender, EventArgs e)
        {
            bool b = richTextBox.SelectedText.Length > 0;

            toolStripButtonCut.Enabled = b;
            toolStripButtonCopy.Enabled = b;
            toolStripButtonSetWordColor.Enabled = b && (!String.IsNullOrEmpty(richTextBox.SelectedText.Trim()));
            toolStripButtonHideSelectedWord.Enabled = b && (!String.IsNullOrEmpty(richTextBox.SelectedText.Trim()));

            toolStripMenuItemCut.Enabled = toolStripButtonCut.Enabled;
            toolStripMenuItemCopy.Enabled = toolStripButtonCopy.Enabled;
            toolStripMenuItemSetWordColor.Enabled = toolStripButtonSetWordColor.Enabled;
            toolStripMenuItemHideSelectedWord.Enabled = toolStripButtonHideSelectedWord.Enabled;
        }

        private void contextMenuStrip_Opening(object sender, System.ComponentModel.CancelEventArgs e)
        {
            contextMenuStrip.Items["toolStripMenuItemSetWordColor"].Enabled = toolStripButtonSetWordColor.Enabled;
            contextMenuStrip.Items["toolStripMenuItemCut"].Enabled = toolStripButtonCut.Enabled;
            contextMenuStrip.Items["toolStripMenuItemCopy"].Enabled = toolStripButtonCopy.Enabled;
            contextMenuStrip.Items["toolStripMenuItemHideSelectedWord"].Enabled = toolStripButtonHideSelectedWord.Enabled;
        }

        private void toolStripButtonCut_Click(object sender, EventArgs e)
        {
            richTextBox.Cut();
        }

        private void toolStripButtonCopy_Click(object sender, EventArgs e)
        {
            richTextBox.Copy();
        }

        private void toolStripButtonPaste_Click(object sender, EventArgs e)
        {
            richTextBox.Paste();
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            LoadParams();
        }

        private void MainForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            m_log.Debug("===== Close app =====");
            SaveParams();
        }
        #endregion

        #region Get text and image from buttons to popupmenu items
        private void SetToolStripMenuItemFromButton(ToolStripButton fromButton, ToolStripMenuItem toMenuItem)
        {
            toMenuItem.Image = fromButton.Image;
            toMenuItem.Text = fromButton.Text;
            toMenuItem.ToolTipText = fromButton.ToolTipText;
        }

        private void InitContextMenuItemsTextAndIcon()
        {
            SetToolStripMenuItemFromButton(toolStripButtonHideSelectedWord, toolStripMenuItemHideSelectedWord);
            SetToolStripMenuItemFromButton(toolStripButtonSetWordColor , toolStripMenuItemSetWordColor);

            SetToolStripMenuItemFromButton(toolStripButtonCut, toolStripMenuItemCut);
            SetToolStripMenuItemFromButton(toolStripButtonCopy, toolStripMenuItemCopy);
            SetToolStripMenuItemFromButton(toolStripButtonPaste, toolStripMenuItemPaste);
        }
        #endregion

        private void SetMinSize()
        {
            Size size = new Size { Width = 0, Height = this.MinimumSize.Height };
            foreach (var item in toolStrip.Items)
            {
                size.Width += ((ToolStripItem)item).Width + toolStrip.GripMargin.Left;
            }
            this.MinimumSize = size;
        }

        private void OpenFile(string fileName)
        {
            richTextBox.Clear();
            toolStripStatusLabelWordsCount.Text = String.Format(m_RemainedWords, 0);
            toolStripStatusLabelTotalWordsRemoved.Text = "";
            this.Text = m_FormCaption;
            try
            {
                richTextBox.LoadFile(fileName, RichTextBoxStreamType.PlainText);
                this.Text = m_FormCaption + ": " + fileName;
            }
            catch (Exception Ex)
            {
                string s = String.Format("Unable to load the file \"{0}\"\n{1}", fileName, Ex.Message);
                m_log.Error(s);
                MessageBox.Show(s, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            ProcessTextAfterOpen();
        }

        private void ProcessTextAfterOpen(bool reCalcWords = false)
        {
            m_log.Debug("Process text after open");

            int count = m_Work.TotalWordsCount;
            if ((count == 0) || (reCalcWords))
            {
                m_Work.SplitTextToWords(richTextBox.Text);
                count = m_Work.TotalWordsCount;
            }
            else
            {
                count = m_Work.TotalWordsCount;
            }

            toolStripStatusLabelWordsCount.Text = String.Format(m_RemainedWords, count);
            toolStripButtonFilter.Enabled = count > 0;
            toolStripStatusLabelTotalWordsRemoved.Text = String.Empty;

            if (!reCalcWords)
            {
                m_AllowRichTextChanges = true;
                RichTextPushChanges(richTextBox.Text);
#if DEBUG
                toolStripStatusLabelChangesCount.Text = string.Format(m_ChangesCount, 0);
#endif
            }
        }

        private void SetSelectedTextColor(string word, Color newColor)
        {
            m_AllowRichTextChanges = false;
            int i = richTextBox.SelectionStart;
            try
            {
                IList<int> list = m_Work.GetAllWordPositions(word, richTextBox.Text);
                foreach (var item in list)
                {
                    richTextBox.Select(item, word.Length);
                    richTextBox.SelectionColor = newColor;
                }
            }
            finally
            {
                richTextBox.SelectionStart = i;
                richTextBox.SelectionLength = word.Length;
                m_AllowRichTextChanges = true;
            }
        }

        private void RichTextPushChanges(string text)
        {
            if (m_AllowRichTextChanges)
            {
                m_RichTextUndoList.Push(text);
#if DEBUG
                if (m_RichTextUndoList.Count > 0)
                {
                    string s = "PUSH: " + m_RichTextUndoList.ElementAt(0).ToString();
                    Trace.WriteLine(s);
                }
                toolStripStatusLabelChangesCount.Text = String.Format(m_ChangesCount, m_RichTextUndoList.Count());
#endif
            }

            RefreshButtons();
        }

        private void RichTextPopChanges()
        {
            Color oldColor = richTextBox.ForeColor;

            if (m_RichTextUndoList.Count <= 0)
                return;
            try
            {
                m_AllowRichTextChanges = false;
                richTextBox.Text = m_RichTextUndoList.Pop();
#if DEBUG
                if (m_RichTextUndoList.Count > 0)
                {
                    string s = "POP: " + m_RichTextUndoList.ElementAt(0).ToString().Substring(0, 15);
                    Trace.WriteLine(s);
                }
#endif
            }
            finally
            {
                toolStripStatusLabelTotalWordsRemoved.Text = String.Empty;
                ProcessTextAfterOpen(true);

                RefreshButtons();
                richTextBox.ForeColor = oldColor;
                
                richTextBox.SelectAll();
                richTextBox.SelectionColor = oldColor;
                richTextBox.SelectionLength = 0;


                m_AllowRichTextChanges = true;
            }

            if (m_RichTextUndoList.Count == 0)
            {
                RichTextPushChanges(richTextBox.Text);
#if DEBUG
                string s = "===== PUSH: " + m_RichTextUndoList.ElementAt(0).ToString().Substring(0, 15);
                Trace.WriteLine(s);
#endif
            }
        }

        private void RefreshButtons()
        {
            toolStripButtonFilter.Enabled = richTextBox.Lines.Length > 0;
            toolStripButtonHideLettersInAllWords.Enabled = richTextBox.Lines.Length > 0;

            toolStripButtonUndo.Enabled = m_RichTextUndoList.Count > 0;
            toolStripButtonPrint.Enabled = richTextBox.Text.Trim().Length > 0;
        }

        #region Load and Save parameters

        private void LoadParams()
        {
            m_AllowRichTextChanges = false;
            try
            {
                #region App parameters
                richTextBox.BackColor = Properties.Settings.Default.TextBackGroundColor;
                richTextBox.Font = Properties.Settings.Default.TextFont;
                richTextBox.ForeColor = Properties.Settings.Default.TextFontColor;

                if (Properties.Settings.Default.IgnoreFormSizeAndPosition)
                {
                    return;
                }

                this.Left = Properties.Settings.Default.MainFormPosX;
                this.Top = Properties.Settings.Default.MainFormPosY;

                if (Properties.Settings.Default.MainFormWidth > 0)
                {
                    this.Width = Properties.Settings.Default.MainFormWidth;
                }

                if (Properties.Settings.Default.MainFormHeight > 0)
                {
                    this.Height = Properties.Settings.Default.MainFormHeight;
                }

                #endregion
            }
            catch (SettingsPropertyNotFoundException ex)
            {
                m_log.Info("Load settings property error. Probably it's a first app start. " + ex.Message);
            }
            finally
            {
                m_AllowRichTextChanges = true;
            }
        }

        private void SaveParams()
        {
            #region App parameters
            Properties.Settings.Default.TextBackGroundColor = richTextBox.BackColor;
            Properties.Settings.Default.TextFont = richTextBox.Font;
            Properties.Settings.Default.TextFontColor = richTextBox.ForeColor;

            Properties.Settings.Default.MainFormPosX = this.Left;
            Properties.Settings.Default.MainFormPosY = this.Top;
            Properties.Settings.Default.MainFormWidth = this.Width;
            Properties.Settings.Default.MainFormHeight = this.Height;
            #endregion

            Properties.Settings.Default.Save();
        }

        #endregion

        private void HideSelectedWord()
        {
            m_AllowRichTextChanges = true;
            try
            {
                RichTextPushChanges(richTextBox.Text);
            }
            finally
            {
                m_AllowRichTextChanges = false;
            }

            string word = richTextBox.SelectedText.Trim();
            int count;
            richTextBox.Text = m_Work.RemoveWordFromTheText(richTextBox.Text, word, out count);

            toolStripStatusLabelTotalWordsRemoved.Text = String.Format(m_RemovedWords, count);
            toolStripStatusLabelWordsCount.Text = String.Format(m_RemainedWords, m_Work.TotalWordsCount);
        }

        private void HideWordsRandomly(int wordsCountToRemove)
        {
            m_AllowRichTextChanges = true;
            RichTextPushChanges(richTextBox.Text);
            m_AllowRichTextChanges = false;

            // Remove N any words randomly
            string strExceptionMessage = String.Empty;
            Cursor oldCursor = richTextBox.Cursor;
            string text = String.Empty;
            int removedWordsCount = 0;
            try
            {
                richTextBox.Cursor = Cursors.WaitCursor;
                try
                {
                    text = m_Work.RemoveWordsFromTheTextRandomly(richTextBox.Text, wordsCountToRemove, out removedWordsCount);
                }
                catch (Exception ex)
                {
                    richTextBox.Cursor = oldCursor;
                    m_log.Error(ex.Message);
                    MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            finally
            {
                richTextBox.Text = text;
                richTextBox.Cursor = oldCursor;
                m_AllowRichTextChanges = true;

                int count = m_Work.TotalWordsCount;
                toolStripStatusLabelWordsCount.Text = String.Format(m_RemainedWords, count);
                toolStripStatusLabelTotalWordsRemoved.Text = String.Format(m_RemovedWords, removedWordsCount);
            }
        }

        private void HideLettersInAllWords(string selectedItem, int hideLettersCount, int minWordLength = 3)
        {
            m_AllowRichTextChanges = true;
            RichTextPushChanges(richTextBox.Text);
            m_AllowRichTextChanges = false;

            string text = string.Empty;
            Cursor oldCursor = richTextBox.Cursor;
            try
            {
                richTextBox.Cursor = Cursors.WaitCursor;
                LettersWorker worker = new LettersWorker();
                switch (selectedItem)
                {
                    case "radioButtonOnlyVowes":
                    {
                        text = worker.HideLetters(richTextBox.Text, minWordLength, hideLettersCount,
                            LettersReplaceTypes.Vowes);
                        break;
                    }
                    case "radioButtonOnlyConsonant":
                    {
                        text = worker.HideLetters(richTextBox.Text, minWordLength, hideLettersCount,
                            LettersReplaceTypes.Consonants);
                        break;
                    }
                    default:
                    {
                            // radioButtonRandomly
                        text = worker.HideLetters(richTextBox.Text, minWordLength, hideLettersCount);
                        break;
                    }
                }
            }
            finally
            {
                richTextBox.Text = text;
                richTextBox.Cursor = oldCursor;
                m_AllowRichTextChanges = true;
            }
        }

        private void PrintText()
        {
            ReportWorker printer = new ReportWorker(richTextBox);
            printer.PrintText();
        }

        #region ToolStripButtons cursor
        private Cursor m_SavedCursor;

        private void ToolStripButton_MouseEnter(object sender, EventArgs e)
        {
            if (m_SavedCursor == null)
            {
                m_SavedCursor = this.Cursor;
                this.Cursor = Cursors.Hand;
            }
        }

        private void ToolStripButton_MouseLeave(object sender, EventArgs e)
        {
            this.Cursor = m_SavedCursor;
            m_SavedCursor = null;
        }
        #endregion
    }
}
