﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.VisualStyles;

namespace WordSkipper
{
    public partial class FormHideLettersParams : FormBaseDialog
    {
        public string SelectedItem { get; private set; }
        public int HideLettersCount { get; private set; }

        public FormHideLettersParams()
        {
            InitializeComponent();
        }

        private void FormReplaceLettersParams_Load(object sender, EventArgs e)
        {
            RadioButton selectedButton = groupBox1.Controls.OfType<RadioButton>().Last();
            selectedButton.Checked = true;
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            RadioButton selectedButton = groupBox1.Controls.OfType<RadioButton>().FirstOrDefault(n => n.Checked);
            if (selectedButton != null) SelectedItem = selectedButton.Name;
            HideLettersCount = Int32.Parse(numericUpDown.Text);
        }
    }
}

