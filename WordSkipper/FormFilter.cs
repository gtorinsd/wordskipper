﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WordSkipper
{
    public partial class FormFilter : FormBaseDialog
    {
        public FormFilter()
        {
            InitializeComponent();
            ActiveControl = this.numericUpDown;
            
        }

        public int GetWordsCount()
        {
            return Int32.Parse(this.numericUpDown.Value.ToString());
        }
    }
}
