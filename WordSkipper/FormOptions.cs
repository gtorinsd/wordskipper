﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WordSkipper
{
    public partial class FormOptions : FormBaseDialog
    {
        public FormOptions()
        {
            InitializeComponent();
            LoadParameters();
        }

        private void LoadParameters()
        {
            checkBoxSaveFormSizeAndPosition.Checked = Properties.Settings.Default.IgnoreFormSizeAndPosition;
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            SaveParameters();
        }

        private void SaveParameters()
        {
            Properties.Settings.Default.IgnoreFormSizeAndPosition = checkBoxSaveFormSizeAndPosition.Checked;
        }
    }
}
